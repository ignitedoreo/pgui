import pygame
import pgui

screen = pygame.display.set_mode([800, 600])

font1 = pygame.font.Font("font1.ttf", 40)
font2 = pygame.font.Font("font1.ttf", 20)
font3 = pygame.font.SysFont("Arial", 30)

text1 = pgui.Text(screen, 0, 0, "One Line Text", font1, (0, 0, 0))
text2 = pgui.Text(screen, 0, 35, '''
Multi
Line
Text
''', font2, (0, 0, 0))

text3 = pgui.Text(screen, 0, 100, "SysFont Text", font3, (0, 0, 0))
text4 = pgui.Text(screen, 300, 0, "Color Text 1", font3, (255, 0, 0))
text5 = pgui.Text(screen, 300, 35, "Color Text 2", font3, (0, 255, 0))
text6 = pgui.Text(screen, 300, 70, "Color Text 3", font3, (0, 0, 255))

button1 = pgui.Button(screen, 300, 100, "Button Example 1", font1, (200, 200, 200), (100, 100, 100), 5, 10)
button2 = pgui.Button(screen, 300, 170, "Button Example 2", font2, (200, 200, 200), (100, 100, 100), 2, 5)
button3 = pgui.Button(screen, 300, 220, "Button Example 3", font3, (200, 200, 200), (100, 100, 100), 2, -1)
button4 = pgui.Button(screen, 300, 300, "Color Button Example 1", font3, (255, 0, 0), (0, 0, 255), 5, 5)
button5 = pgui.Button(screen, 300, 370, "Color Button Example 2", font3, (0, 0, 255), (0, 255, 0), 5, 5)
button6 = pgui.Button(screen, 300, 440, "Color Button Example 3", font3, (0, 255, 0), (255, 0, 0), 5, 5)

running = True
while running:
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            running = False
        if i.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            if button1.isCollide(x, y):
                text3.setText("You clicked button1!")
                text3.color = (255, 0, 0)
                text3.font = font1
            if button2.isCollide(x, y):
                text3.setText("You clicked button2!")
                text3.color = (0, 0, 0)
                text3.font = font2
            if button3.isCollide(x, y):
                text3.setText("You clicked button3!")
                text3.color = (0, 255, 0)
                text3.font = font3
            if button4.isCollide(x, y):
                text3.setText("You clicked button4!")
                text3.color = (255, 0, 255)
                text3.font = font1
            if button5.isCollide(x, y):
                text3.setText("You clicked button5!")
                text3.color = (255, 255, 0)
                text3.font = font2
            if button6.isCollide(x, y):
                text3.setText("You clicked button6!")
                text3.color = (0, 255, 255)
                text3.font = font3
    screen.fill((255, 255, 255))

    text1.draw()
    text2.draw()
    text3.draw()
    text4.draw()
    text5.draw()
    text6.draw()

    button1.draw()
    button2.draw()
    button3.draw()
    button4.draw()
    button5.draw()
    button6.draw()

    pygame.display.flip()

pygame.quit()