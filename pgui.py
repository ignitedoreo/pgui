from cgitb import text
from operator import truediv
import pygame
pygame.init()

class Text:
    def __init__(self, surface: pygame.surface.Surface, x: int, y: int, text: str, font: pygame.font.Font, color: tuple) -> None:
        self.surface = surface
        self.text = text.split("\n")
        for k, i in enumerate(self.text):
            if i == '':
                self.text.pop(k)
        self.font = font
        self.color = color
        self.x = x
        self.y = y

    def draw(self) -> None:
        yOffset = 0
        for i in self.text:
            textSurface = self.font.render(i, True, self.color)
            self.surface.blit(textSurface, (self.x, self.y+yOffset))
            yOffset += self.font.size(i)[1]

    def getSize(self) -> tuple:
        return self.font.size(self.text)

    def setText(self, newText: str) -> None:
        self.text = newText.split("\n")
        for k, i in enumerate(self.text):
            if i == '':
                self.text.pop(k)

class Button:
    def __init__(self, surface: pygame.surface.Surface, x: int, y: int, text: str, font: pygame.font.Font, colorInner: tuple, colorBorder: tuple, rectBorderWidth: int = 5, rectRound: int = 10) -> None:
        self.surface = surface
        self.text = text
        self.font = font
        self.colorInner = colorInner
        self.colorBorder = colorBorder
        self.rectBorderWidth = rectBorderWidth
        self.rectRound = rectRound
        self.width, self.height = self.font.size(self.text)
        self.width += 40
        self.height += 20
        self.x = x
        self.y = y
    
    def draw(self) -> None:
        pygame.draw.rect(self.surface, self.colorInner, (self.x, self.y, self.width, self.height), 0, self.rectRound)
        pygame.draw.rect(self.surface, self.colorBorder, (self.x, self.y, self.width, self.height), self.rectBorderWidth, self.rectRound)
        text = self.font.render(self.text, True, self.colorBorder)
        self.surface.blit(text, (self.x+(self.width//2)-(text.get_size()[0]//2), self.y+(self.height//2)-(text.get_size()[1]//2)))
    
    def isCollide(self, x: int, y: int) -> bool:
        if x > self.x and y > self.y and x < self.x + self.width and y < self.y + self.height:
            return True
        else:
            return False