This README available in [Russian Language](https://codeberg.org/ignitedoreo/pgui/src/branch/main/README_ru.md).
# PgUI - UI in PyGame made simple!
PgUI (PyGame User Interface) - mini library made by me for making ui in pygame.
# Usage
## Test app
For seeing how it works in action this repo contains `test.py` file with example program.  
You can run this file or see it's code.
## Adding it to project
No PyPI now, sorry ;(  
To add this library to your project:
1. Copy `pgui.py` to your project folder.
2. Add `import pgui` on top of your main file.
</a>
## How to use
Now it's only 2 classes in this lib. I already said it is small.
### Text class
Text rendering in pygame is complicated.
Like, look at this:
```
text = font.render("some text blah blah blah", True, (0, 0, 0))
screenSurface.blit(text, (x, y))
```
I, personaly, think it's too overcomplicated. In other frameworks it's alot simpler. Like Love2D for lua:
```
function love.draw()
    love.graphics.print("text", x, y)
end
```
It is alot simpler.
So in PgUI it looks like this:
```
# On program init:
text = pgui.Text(screenSurface, x, y, "text", font, (r, g, b))
# In loop:
text.render()
```
Also it's easy to change text mid program:
```
text.setText("new text")
```
And also it supports multiple lines out of box!
So this:
```
# On program init:
text = pgui.Text(screenSurface, x, y, "text\ntext\ntext", font, (r, g, b))
# In loop:
text.render()
```
Will work as intended.
### Button class
With PgUI buttons made simple!
Let's create one:
```
# On program init:
button = pgui.Button(screen, 300, 100, "text on button.", font, innerColor, borderColor, borderWidth, borderRound)
# In loop:
button.draw()
```
We created it. How can we do something if user clicks it?
It's simple!
```
# Event loop
if i.type == pygame.MOUSEBUTTONDOWN:
    x, y = pygame.mouse.get_pos()
    if button.isCollide(x, y):
        text.setText("You clicked button! :)")
```
It is alot simpler than `x > x and y > y and x < x+width and y < y+height`.
# Contributing
If you will meet any bugs or wanna suggest feature use Issues tab.  
If you wanna contribute than create issue where you explain why and how. Then if I will agree do a pull request.  
Have a great day!  